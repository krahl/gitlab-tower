const cron = require("node-cron");

module.exports.bootstrap = function(cb) {
  cron.schedule('*/5 * * * *', function() {
      updateService.start();
  }, true);
  //Validar Updates
  cron.schedule('0 8 * * *', function() {
    krahlLabs.updateSettings();
  }, true);
  krahlLabs.updateSettings();
  cb();
};
