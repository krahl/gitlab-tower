## Installation &nbsp;
**With Docker:**
```sh
# Download latest image and run the project
$ docker run -p 80:1337 registry.gitlab.com/krahl/gitlab-tower:latest
# To save the data it is necessary to create volume in .tmp
$ docker run -p 80:1337 -v yourbkpdir/.tmp:/var/www/.tmp registry.gitlab.com/krahl/gitlab-tower:latest
```

**With Node.js:**
```sh
# Download this project
$ git clone -b master https://gitlab.com/krahl/gitlab-tower.git
# Install dependencies
$ npm install
# And run the project
$ npm start
```

## Configuration Requirements

1. **Gitlab CE 11.0 or higher (Gitlab EE not tested).**
2. **Gitlab API Token from a Gitlab Admin account, since only Admin can fetch information about runners.**