const request = require("request");

module.exports = {
  getAllRunners: () => {
    return new Promise( (success, reject) => {
      settingService.get().then( (settings) => {
        let url = `${settings.gitlab_api}/runners/all?per_page=100&private_token=${settings.gitlab_token}`;

        request.get(url, (err, response, body) => {
          if (err) return reject({status: "getAllRunners - Falha ao buscar os dados no Gitlab!"});

          if (response.statusCode && response.statusCode == 200 ){
            return success(JSON.parse(body));
          }else{
            return reject(body);
          }
        });

      }).catch( (err) => {
        return reject({status: "Falha ao buscar as configurações do projeto!"});
      });
    });
  },
  getRunner: (gitlabID) => {
    return new Promise( (success, reject) => {
      settingService.get().then( (settings) => {
        let url = `${settings.gitlab_api}/runners/${gitlabID}?private_token=${settings.gitlab_token}`;

        request.get(url, (err, response, body) => {
          if (err) return reject({status: "getRunner - Falha ao buscar os dados no Gitlab!", url: url, body: JSON.stringify(body), response: JSON.stringify(response),err: JSON.stringify(err)});

          if (response.statusCode && response.statusCode == 200 ){
            return success(JSON.parse(body));
          }else{
            return reject(body);
          }
        });

      }).catch( (err) => {
        return reject({status: "Falha ao buscar as configurações do projeto!"});
      });
    });
  },
  getVersion: () => {
    return new Promise( (success,reject) => {
      settingService.get().then( (settings) => {
        let url = `${settings.gitlab_api}/version?private_token=${settings.gitlab_token}`;

        request.get(url, (err, response, body) => {
          if (err) return reject({status: "getVersion - Falha ao buscar os dados no Gitlab!"});

          if (response.statusCode && response.statusCode == 200 ){
            return success(JSON.parse(body));
          }else{
            return reject(body);
          }
        });

      }).catch( (err) => {
        return reject({status: "Falha ao buscar as configurações do projeto!"});
      });
    });
  }
}
