
module.exports = {
  start: () => {
    gitlabService.getAllRunners().then( (allRunners) => {
      let allRunnersIds = sails.util.pluck(allRunners, 'id');
      
      Runner.find().exec( (err, runners) => {
        let towerRunnersIds = sails.util.pluck(runners, 'gitlab_id');

        allRunnersIds.forEach( (gitlabID) => {
          if (towerRunnersIds.indexOf(gitlabID) != -1){//Atualiza os que encontrou
            gitlabService.getRunner(gitlabID).then( (runner) => {
              updateService.mapRunnerAtt( runner ).then( (mappedRunner) => {
                Runner.find({gitlab_id: gitlabID}).exec( (err, runnerFind) => {
                  if (runnerFind[0] && runnerFind[0].id){
                      Runner.update(runnerFind[0].id, mappedRunner, (err, updated) =>{
                        if(err) console.log(err);
                      });
                  }
                });
              }).catch( (err) => {
                console.log(err);
              });
            }).catch( (err) => {
              console.log(err);
            });
          }else{//Cria o que não encontrou
            gitlabService.getRunner(gitlabID).then( (runner) => {
              updateService.mapRunnerAtt( runner ).then( (mappedRunner) => {
                Runner.create(mappedRunner, (err, created) => {
                  if(err) console.log(err);
                });
              }).catch( (err) => {
                console.log(err);
              });
            }).catch( (err) => {
              console.log(err);
            });
          }

          towerRunnersIds.forEach( (towerId) => {//Marca como inativo o que só existe aqui
            if (allRunnersIds.indexOf(towerId) == -1 ){
              Runner.find({gitlab_id: towerId}).exec( (err, runnerFind) => {
                if (runnerFind[0] && runnerFind[0].id){
                    Runner.update(runnerFind[0].id, {valid: false}, (err, updated) =>{
                      if(err) console.log(err);
                    });
                }
              });
            }
          })
        });
      });
		}).catch( (err) => {
      console.log(err)
			return;
		});
  },
  mapRunnerAtt: ( runner ) => {
    return new Promise( (success, reject) => {
      try {
        let runnerMaped = {
          gitlab_id: runner.id,
          description: runner.description,
          ip_address: runner.ip_address,
          active: runner.active,
          is_shared: runner.is_shared,
          name: runner.name,
          online: runner.online,
          status: runner.status,
          tag_list: runner.tag_list.toString(),
          run_untagged: runner.run_untagged,
          locked: runner.locked,
          maximum_timeout: runner.maximum_timeout,
          access_level: runner.access_level,
          version: runner.version,
          revision: runner.revision,
          platform: runner.platform,
          architecture: runner.architecture,
          contacted_at: runner.contacted_at,
          token: runner.token,
          projects: sails.util.pluck(runner.projects,'id').toString(),
          groups: sails.util.pluck(runner.groups,'id').toString(),
          valid: true
        }
        return success(runnerMaped);
      } catch (e) {
        console.log(e);
        return reject({});
      } finally {

      }
    });
  }
}
