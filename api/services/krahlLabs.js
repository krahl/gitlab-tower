const request = require("request");
const labsFile = "https://krahl.gitlab.io/krahl-site/update.json";

module.exports = {
    check: () => {
        return new Promise( (success, reject) => {
            try {
                request.get(labsFile, (err, response, body) => {
                    if (err) return reject({status: "Falha ao verificar Updates!"});
        
                    if (response.statusCode && response.statusCode == 200 ){
                        return success(JSON.parse(body));
                    }else{
                        return reject(body);
                    }
                });    
            } catch (error) {
                return reject(error);
            }
            
        });        
    },
    validade: () => {
        const compareVersions = require('compare-versions');
        const localVersion = require("../../package.json").version;
        return new Promise( (success,reject) => {
            try {
                krahlLabs.check().then( (dados) => {
                    if (dados && dados["gitlab-tower"]){

                        return success({ result: compareVersions(dados["gitlab-tower"], localVersion), remote: dados["gitlab-tower"], local: localVersion });
                    }else{
                        return success({result: -2});
                    }
                    
                });    
            } catch (error) {
                return reject(-1);
            }
            
        });
    },
    updateSettings: () => {
        settingService.get().then( (setting) => {
            krahlLabs.validade().then( (dados) => {
                infoService.setTowerUpdate( (dados.result > 0) );
                infoService.setTowerRemoteVersion(dados.remote);
                Setting.update(1, {towerVersion: dados.remote,towerHasUpdate: (dados.result > 0)}).exec( (err, set)=>{});
            });
        });
    }
}