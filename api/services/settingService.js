const request = require("request");

module.exports = {
  get: () => {
    return new Promise( (success, reject) => {
      Setting.findOne(1).exec( (err, setting) => {
        if (err || !setting) return reject({});
        return success(setting);
      })
    });
  },
  update: (gitlab_api, gitlab_token, password) => {
    return new Promise( (success, reject) => {

      if (gitlab_api && gitlab_token && password){
        let url = `${gitlab_api}/version?private_token=${gitlab_token}`;

        request.get(url, (err, response, body) => {
          if (err) return reject(true);

          if (response.statusCode && response.statusCode == 200 ){
            let version = JSON.parse(body).version;
            Setting.findOne(1).exec( (err, settings) => {
              if (err || !settings){
                  Setting.create({gitlab_api:gitlab_api, gitlab_token:gitlab_token, gitlab_version:version, password: password}).exec( (err, s) => {
                      updateService.start();
                      return success(true);
                  });
              }else{
                Setting.update(1,{gitlab_api:gitlab_api, gitlab_token:gitlab_token, password: password}).exec( (err, s) => {
                  updateService.start();
                  return success(true);
                });
              }
            });
          }else{
            return reject(true);
          }
        });
      }else{
        return reject(true);
      }

    });
  }
}
