let towerUpdate = false;
let towerVersion = require("../../package.json").version;
let towerRemoteVersion = require("../../package.json").version;

module.exports = {
    setTowerVersion: (v) => {
        towerVersion = v;
    },
    setTowerUpdate: (v) => {
        towerUpdate = v;
    },
    setTowerRemoteVersion: (v) => {
        towerRemoteVersion = v;
    },
    getTowerVersion: () => {
        return towerVersion;
    },
    getTowerUpdate: () => {
        return towerUpdate;
    },
    getTowerRemoteVersion: () => {
        return towerRemoteVersion;
    }
}