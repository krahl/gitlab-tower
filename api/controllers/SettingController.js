const bcrypt = require('bcryptjs');

module.exports = {
  configure: (req,res) => {
    Setting.findOne(1).exec( (err, config) => {
      if (err || !config) return res.view({config: null});
      return res.view({config: config});
    });
  },
  afterRegister: (req, res) => {
    let gitlab_api = req.param("gitlab_api");
    let gitlab_token = req.param("gitlab_token");
    let password = req.param("password");

    settingService.get().then( (settings) => {
      settingService.update(gitlab_api, gitlab_token, password).then( () => {
        return res.redirect('/')
      }).catch( () => {
        return res.redirect('/setting/configure');
      });
    }).catch((er) => {
      settingService.update(gitlab_api, gitlab_token, password).then( () => {
        req.session.authenticated = true;
        return res.redirect('/')
      }).catch( () => {
        return res.redirect('/register');
      });
    });    

    
  },
  login: (req,res) => {
    settingService.get().then( (settings) => {
      return res.view({layout:''});
    }).catch((er) => {
      return res.redirect('/register');
    });    
  },
  auth: (req,res) => {
    let password = req.param('password');

    if (password){
      settingService.get().then( (settings) => {
        if (bcrypt.compareSync(password, settings.password)){
          req.session.authenticated = true;
          return res.redirect('/');
        }else{
          req.session.authenticated = false;
          return res.redirect('/login');
        }
        
      }).catch( (err) => {
        return res.redirect('/login');
      });
    }else{
      return res.redirect('/login');
    }
  },
  logout: (req,res) => {
    req.session.authenticated = false;
    return res.redirect('/login');
  },
  register: (req,res) => {
    settingService.get().then( (settings) => {
      return res.redirect('/login');
    }).catch((er) => {
      return res.view({layout:''});
    });    
  }
};
