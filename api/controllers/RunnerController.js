const compareVersions = require('compare-versions');
const moment = require('moment');

module.exports = {
	index: (req,res) => {
		settingService.get().then( (settings) => {
			Runner.find().sort('description').exec( (err, runners) => {
				return res.view({runners: runners, compareVersions:compareVersions, gitlabVersion: settings.gitlab_version,moment:moment});
			});
		}).catch( (err) => {
			return res.redirect('/register');
		});
	},
	all: (req,res) => {
		gitlabService.getAllRunners().then( (ret) => {
			return res.json(ret);
		}).catch( (err) => {
			return res.json(err);
		})
	},
	dashboard: (req,res) => {
		settingService.get().then( (settings) => {
			Runner.count().exec( (err, countTotal) => {
				Runner.count({status: ['online','active']}).exec( (err, countOnline) => {
					Runner.count({status: ['offline','paused','not_connected']}).exec( (err, countOffline) => {
						Runner.count({platform: 'linux'}).exec( (err, countLinux) => {
							Runner.count({platform: 'windows'}).exec( (err, countWin) => {
								Runner.count({is_shared: true}).exec( (err, countShared) => {
									Runner.count({tag_list: {'contains' : 'docker'}}).exec( (err, countDocker) => {
										Runner.find().exec( (err, findR) => {
											let ips = sails.util.pluck(findR,'ip_address').reduce(function(a,b){if(a.indexOf(b)<0)a.push(b);return a;},[])
											return res.view({ total: countTotal, online: countOnline, offline: countOffline, gitlabVersion: settings.gitlab_version, countLinux:countLinux,countWin:countWin,countShared:countShared,countDocker:countDocker, maquinas: ips.length});
										});		
									});
								});
							});
						});
					});
				});
			});
		}).catch( (err) => {
			return res.redirect('/register');
		})
		
	}
};
