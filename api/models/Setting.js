const bcrypt = require('bcryptjs');

module.exports = {
  attributes: {
    gitlab_api: {type: 'string', required: true},
    gitlab_token: {type: 'string', required: true},
    gitlab_version: {type: 'string', defaultsTo: '11.0'},
    password: {type: 'string', required: true},
    towerVersion: {type: 'string'},
    towerHasUpdate: {type: 'boolean', defaultsTo: false},
  },
  beforeCreate: function (user, cb) {
    if (user.password) {
      var salt = bcrypt.genSaltSync(10);
      var hash = bcrypt.hashSync(user.password, salt);
      user.password = hash;
      cb();
    } else {
      cb();
    }
  },
  beforeUpdate: function (user, cb) {
    if (user.password) {
      var salt = bcrypt.genSaltSync(10);
      var hash = bcrypt.hashSync(user.password, salt);
      user.password = hash;
      cb();
    } else {
      cb();
    }
  }
};