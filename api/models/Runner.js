module.exports = {
  attributes: {
    active: {type: 'boolean', defaultsTo: true},
    architecture: {type: 'string'},
    description: {type: 'string'},
    gitlab_id: {type: 'integer'},
    ip_address: {type: 'string'},
    is_shared: {type: 'boolean', defaultsTo: false},
    contacted_at: {type: 'string'},
    name: {type: 'string'},
    online: {type: 'boolean'},
    status: {type: 'string'},
    platform: {type: 'string'},
    projects: {type: 'json'},
    token: {type: 'string'},
    revision: {type: 'string'},
    tag_list: {type: 'string'},
    version: {type: 'string'},
    version_to_check: {type: 'float'},
    access_level: {type: 'string'},
    maximum_timeout: {type: 'string'},
    run_untagged: {type: 'boolean', defaultsTo: false},
    locked: {type: 'boolean', defaultsTo: false},
    groups: {type: 'string'},
    valid: {type: 'boolean', defaultsTo: true}//Existe no Gitlab em questão
  }
};
